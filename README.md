# example-go-docker-gitlab

This repo contains source code for the article: https://dev.to/hypnoglow/how-to-make-friends-with-golang-docker-and-gitlab-ci-4bil


$ go test ./...

$ go build -o app .
$ ./app

# and in another terminal:
$ curl http://localhost:8080/greeting
Hello, world!


Question: how we will pass dependencies to the docker build process?

Option 1: pass vendor directory along with the source code using COPY command.

Option 2: install dependencies on build time using RUN dep ensure.

this tut uses 1

we create a job dep to install dependencies and 
    store vendor directory as a GitLab artifact. 

In other jobs, we add dep job as a dependency, 
    and GitLab will extract previously stored vendor right into our project directory.

..

